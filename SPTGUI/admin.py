## The admin views
## MW, GPLv3+

import os, shutil

from django.contrib import admin
from django.conf.urls import url
from django.utils.html import format_html
from django.core.urlresolvers import reverse
from django.template import loader
from django.http import HttpResponse
from django.shortcuts import redirect
from .models import Analysis, Dataset, Download, Email

## ==== Constants
bf = "./static/analysis/" ## Location to save the fitted datasets

## ==== Functions
def clean_files():
    """Cleans the analysis files without database entry. This can be heavy"""
    [shutil.rmtree(os.path.join(bf,f)) for f in clean_files_sim()]

def clean_files_sim():
    """Return a list of folders to be deleted.
    Does not perform the actual deletion"""
    na_ana = [an.url_basename for an in Analysis.objects.all()]
    na_fil = os.listdir(bf)
    na_torem = [f for f in na_fil if f not in na_ana]
    return na_torem

## ==== Models
class AnalysisAdmin(admin.ModelAdmin):
    list_display = ('url_basename', 'pub_date', 'acc_date', 'count_datasets',
                    'size_datasets', 'count_analyses', 'size_analyses', 'is_demo',
                    'analysis_actions')
    readonly_fields = ('analysis_actions',)
    actions = ('clean_files_sim', 'clean_files')
    
    def get_urls(self):
        urls = super(AnalysisAdmin, self).get_urls()
        custom_urls = [
            url(
                r'^(?P<analysis_id>.+)/test/$',
                self.admin_site.admin_view(self.run_tests),
                name='analysis-test',
            ),
        ]
        return custom_urls + urls
    
    def analysis_actions(self, obj):
        r_tests = format_html(
            '<a class="button" href="{}">Run tests</a>&nbsp;',
            reverse('admin:analysis-test', args=[obj.pk]))
        return r_tests
    
    def run_tests(self, request, analysis_id, *args, **kwargs):
        ana = Analysis.objects.get(id=analysis_id)
        template = loader.get_template('SPTGUI/analysis.html')
        return redirect('SPTGUI:analysis_dbg', url_basename=ana.url_basename)

    def clean_files_sim(self, request, queryset):
        tdl = clean_files_sim()
        msg = "The following {} unused folder(s) will be deleted: ".format(len(tdl))
        self.message_user(request, msg+" ".join(tdl))
        
    def clean_files(self, request, queryset):
        try:
            tdl = clean_files_sim()
            clean_files()
            self.message_user(request, "{} unused folder(s) have been deleted".format(len(tdl)))
        except e:
            self.message_user(request, "It failed miserably: {}".format(e))

    clean_files.short_description = "/!\ Delete files from deleted analyses /!\."
    clean_files_sim.short_description = "List files from deleted analyses."
    analysis_actions.short_description = 'Actions'
    analysis_actions.allow_tags = True

class EmailAdmin(admin.ModelAdmin):
    list_display = ('email', 'add_date', 'validated')

# Register your models here.
admin.site.register(Analysis, AnalysisAdmin)
admin.site.register(Email, EmailAdmin)
admin.site.register(Dataset)
admin.site.register(Download)
